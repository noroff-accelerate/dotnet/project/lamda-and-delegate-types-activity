# Space Observatory Data Processing

## Overview
This C# program showcases the use of lambda functions and various types of delegates (`Func`, `Predicate`, `Action`) in processing astronomical data. The activity is designed to enhance understanding and practical application of these concepts in a simulated space observatory context.

## Features
- **Star Data Structure**: Defines a class `StarData` for handling astronomical data.
- **Star Catalog Initialization**: Creates a catalog of stars with sample data.
- **Lambda Functions for Analysis**: Utilizes lambda functions for complex calculations like average brightness.
- **Func Delegate**: Implements `Func` for distance conversions and calculations.
- **Predicate Delegate**: Employs `Predicate<StarData>` for filtering stars based on distance criteria.
- **Action Delegate**: Utilizes `Action<StarData>` for logging star details.
- **Data Processing Simulation**: Integrates various delegates and lambda functions for a cohesive data processing workflow.

## Implementation Details

### Star Data Structure
`StarData` class includes properties for the star's name, distance, and brightness.

### Star Catalog
A `List<StarData>` named `starCatalog` is populated with a variety of star data.

### Lambda Functions
A lambda function calculates the average brightness of stars in the catalog.

### Func for Calculations
`Func<double, double>` is used to convert light years to kilometers for distance calculations.

### Predicate for Filtering
`Predicate<StarData>` identifies stars within a specified distance range.

### Action for Logging
`Action<StarData>` logs each star's details to the console.

### Data Processing
The program simulates a data processing scenario, combining `Func`, `Predicate`, and `Action` for practical application.

## Usage
Run the program to observe the processing of star data in the console. The output will include logged details of each star, calculations of distances, and the application of filtering criteria.

## License
This project is licensed under the [MIT License](LICENSE).
