﻿namespace Noroff.Samples.LambdaDelegateTypesActivity
{
    class Program
    {
        static void Main(string[] args)
        {
            // Sample astronomical data
            List<StarData> starCatalog = new List<StarData>
            {
                new StarData("Alpha Centauri", 4.37, 1.56),
                new StarData("Barnard's Star", 5.96, 0.45),
                new StarData("Sirius", 8.6, 1.71),
                // Additional stars...
            };

            // 1. Lambda Functions for Data Analysis
            // Calculating average brightness using lambda expression
            Func<List<StarData>, double> averageBrightness = stars => stars.Average(star => star.Brightness);
            double avgBrightness = averageBrightness(starCatalog);
            Console.WriteLine($"Average Brightness: {avgBrightness}");

            // 2. Using Func for Complex Calculations
            // Lambda to calculate light years to kilometers
            Func<double, double> lightYearsToKm = lightYears => lightYears * 9.461e+12;
            double distanceInKm = lightYearsToKm(starCatalog.First().Distance);
            Console.WriteLine($"Distance in Km: {distanceInKm}");

            // 3. Data Filtering with Predicate
            // Predicate to identify stars within 10 light years
            Predicate<StarData> isNearbyStar = star => star.Distance <= 10;
            var nearbyStars = starCatalog.FindAll(isNearbyStar);
            Console.WriteLine("Nearby Stars:");
            nearbyStars.ForEach(star => Console.WriteLine(star.Name));

            // 4. Action Delegate for Data Logging
            // Action to log star data
            Action<StarData> logStarData = star =>
            {
                Console.WriteLine($"Logging: {star.Name}, Distance: {star.Distance}, Brightness: {star.Brightness}");
            };
            starCatalog.ForEach(logStarData);

            // 5. Integrating Delegates in the Observatory System
            // Simulating a complete data processing scenario
            Console.WriteLine("\nComplete Data Processing:");
            starCatalog.ForEach(star =>
            {
                logStarData(star); // Logging each star data
                double distInKm = lightYearsToKm(star.Distance);
                Console.WriteLine($"{star.Name} is {distInKm} km away.");
            });
        }
    }

    // Data structure for star information
    public class StarData
    {
        public string Name { get; set; }
        public double Distance { get; set; } // In light years
        public double Brightness { get; set; } // Arbitrary brightness value

        public StarData(string name, double distance, double brightness)
        {
            Name = name;
            Distance = distance;
            Brightness = brightness;
        }
    }
}